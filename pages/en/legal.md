---
layout: page
title: Legal notice
permalink: /en/legal
lang: en
ref: legal
---


The site [https://shop.crafting-labs.fr](https://shop.crafting-labs.fr) is edited by Antoine Vernois, and hosted on [GitLab](https://gitlab.com).

Credit card payment are operated by [Stripe](https://stripe.com).

Directeur de la publication : Antoine Vernois.

