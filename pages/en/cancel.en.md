---
layout: page
title: Canceled transaction
permalink: /en/cancel
lang: "en"
ref: "cancel"
---

{% assign contact_page = site.pages | where:"ref", "contact" | where: "lang", page.lang | first %}

The transaction was cancelled.

If that's not what you meant to do, please feel free to [contact]({{ contact_page.url }}) so we can figure out what went wrong.