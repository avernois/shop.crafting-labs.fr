---
layout: page
title: Success \o/
permalink: /en/success
lang: "en"
ref: "success"
---

{% assign contact_page = site.pages | where:"ref", "contact" | where: "lang", page.lang | first %}

The transaction was successful. Thanks for your order \o/

I will contact you soon to give you an update on its status.


If you have any questions, feel free to [contact me]({{ contact_page.url }}).
