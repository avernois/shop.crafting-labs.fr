---
layout: page
title: Privacy
permalink: /en/privacy
lang: en
ref: privacy
---

## Personal data

The site [{{ site.title }}]({{ site.url }}) does not collect personal data when browsing.

When you place an order, your name, billing / delivery addresses and e-mail address are collected by [Stripe] (htps: //stripe.com), the payment intermediary, for the proper processing of the order. 

You can learn more about Stripe privacy policy on their [Global Privacy Policy](https://stripe.com/en-fr/privacy) page.

## Cookies

The site does not use cookies directly.

However [Stripe](https://stripe.com) may use some cookies to ensure its services.
You can learn more about Stripe cookies policy on their [Cookies Policy](https://stripe.com/cookies-policy/legal) page.



