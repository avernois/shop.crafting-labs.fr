---
layout: page
title: Transaction annulée
permalink: /fr/cancel
lang: "fr"
ref: "cancel"
---

{% assign contact_page = site.pages | where:"ref", "contact" | where: "lang", page.lang | first %}

La transaction a été annulée.

Si ce n'est pas ce que vous souhaitiez faire, n'hésitez pas à me [contacter]({{ contact_page.url }}).