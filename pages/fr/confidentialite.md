---
layout: page
title: Confidentialité
permalink: /fr/confidentialite
lang: "fr"
ref: "privacy"
---

## Données personnelles

Le site [{{ site.title }}]({{ site.url }}) ne collecte pas de données personnelles lors de la navigation.

En cas de commande, vos nom, adresse de facturation/livraison et e-mail sont collectés par [Stripe](htps://stripe.com), l'intermédiare de paiement, pour le bon traitement de la commande.

Vous pouvez en apprendre plus sur la politique de confidentialité de Stripe sur leur page [Global Privacy Policy](https://stripe.com/en-fr/privacy).

## Cookies

Ce site n'utilise pas de cookies directement.

Cependant, [Stripe](https://stripe.com) peut en utiliser pour assurer son fonctionnement.
Vous pouvez en apprendre plus sur leur fonctionnement sur leur site [Cookies Policy](https://stripe.com/cookies-policy/legal)
