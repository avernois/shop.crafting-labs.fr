---
layout: page
title: Mentions légales
permalink: /fr/mentions-legales
lang: "fr"
ref: "legal"
---


Le site [https://shop.crafting-labs.fr](https://shop.crafting-labs.fr) est édité par Antoine Vernois, entreprise individuelle, et hébergé par [GitLab](https://gitlab.com).

Les paiements par carte bancaire sont effectués via [Stripe](https://stripe.com).

Directeur de la publication : Antoine Vernois.

