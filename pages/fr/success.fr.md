---
layout: page
title: Succès \o/
permalink: /fr/success
lang: "fr"
ref: "success"
---

{% assign contact_page = site.pages | where:"ref", "contact" | where: "lang", page.lang | first %}


Votre paiement a réussi. Merci. \o/

Je vous recontacte très prochainement pour vous donner les infos sur les informations sur la commande.


Si vous avez des questions complémentaires, n'hésitez pas à me [contacter]({{ contact_page.url }}).