---
layout: product
title: "Dégaine"
category: "countdown"
lang: en
ref: degaine
---


*Dégaine* is a two-sided countdown timer. 

I initially built it to help tracking time when giving a conference or facilitating a workshop.
The remaining time is displayed on both side making it easily available to everyone in the room.


## features:

- Countdown timer duration from 0 to 99 minutes (default set to 45 minutes).
- By default, both sides display the same information.
- The display color changes to mark 50%, 90% and 100% time elapsed.
- By default, time is displayed both side. But it can be configured to only appears on one side.
- full arduino nana available by opening the casing.
- a single button that can be turn and push.


## open source

The entire project (source code, box plan and electronic diagram) is under an open source license and available on [gitlab](https://gitlab.com/avernois/degaine).

Contributions are welcome and playing with the platform is strongly encouraged.


## building and building time

*Dégaine* is handmade by me in my workshop on demand. Depending on whether I have the components in stock and other projects in progress, manufacturing times can vary from a few days to 2 months.

## technical details

* power supply: it requires a 5v 500mA power supply with microUSB connector. The power supply is not provided.
* wifi: 802.11 b/g/n (2.4gHz only)
* dimension : 152mm x 100mm x 33mm
* poids : 195g
* materials: 
  * casing and internal structure are made of birch plywood
  * the light diffuser (between leds and letters) is made of recycled paper