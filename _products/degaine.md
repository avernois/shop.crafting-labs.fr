---
layout: product
title: "Dégaine"
category: "compte à rebours"
lang: fr
ref: degaine
---

*Dégaine* est un compte à rebours électronique à deux faces.

Il est conçu pour m'aider à suivre le temps lorsque je donne une conférence ou que j'anime un atelier. L'information est dupliquée de chaque côté la rendant plus facilement accessible à l'ensemble des participant⋅e⋅s.

## descriptif :

- Durée du compte à rebours de 0 à 99 minutes (défaut à 45 minutes)
- Affichage identique des 2 côtés
- Changement de couleur pour signaler 50%, 90% et 100% du temps écoulés
- Choix du côté d'affichage (les 2 par défaut)
- arduino nano complet accessible en ouvrant le boitier
- un unique bouton, qu'on pousse et qu'on tourne

## open source

L’ensemble du projet (code source, plan du boitier et schéma électronique) est sous licence open source et disponible sur [gitlab](https://gitlab.com/avernois/degaine).

Les contributions sont les bienvenues et jouer avec la plateforme est fortement encouragé.

## fabrication et délais

*Dégaine* est fabriqué par mes soins dans mon atelier à la demande. Selon si j'ai les composants en stock et les autres projets en cours, les délais de fabrication peuvent varier de quelques jours à 2 mois.

## caractéristiques techniques

* alimentation : 5v 500mA (ou supérieur) avec un connecteur micro USB. L'alimentation n'est pas fournie.
* dimension : 152mm x 100mm x 33mm
* poids : 195g
* matériaux :
  * le boitier et la structure interne sont en contreplaqué de bouleau
  * le diffuseur (entre les leds et les lettres) est en papier recyclé