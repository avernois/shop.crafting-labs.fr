---
layout: product
title: "Triangle"
category: wifi clock
lang: en
ref: triangle
---

*Triangle* is a clock that gives time using colored triangles.


Configurable, it sets itself on time and follows timetables (like daylight saving time) changes automatically via a wifi connection to the internet.

It's made of 36 leds, but only 27 of them are used to tell time. The other 9 (the inner triangle) can be controlled using an HTTP api.

Other features may be added over time, but there are no guarantees about that.

## open source

The entire project (source code, box plan and electronic diagram) is under an open source license and available on [gitlab](https://gitlab.com/avernois/clocks).

The automatic time update via the NTP protocol according to the configured time zone is based on the [ZoneTransition](https://gitlab.com/avernois/zone-transition) project.
[ZoneTransition](https://gitlab.com/avernois/zone-transition) manages the offset from UTC time and the dates of summer / winter time changes.

Contributions are welcome and playing with the platform is strongly encouraged. For this, the pins allowing the reprogramming of the micro controller are accessible (requires a usb <-> serial converter not included). 

## building and building time

*Triangle* is handmade by me in my workshop on demand. Depending on whether I have the components in stock and other projects in progress, manufacturing times can vary from a few days to 2 months.

## technical details

* power supply: it requires a 5v 500mA power supply with microUSB connector. The power supply is not provided.
* wifi: 802.11 b/g/n (2.4gHz only)
* dimension : 192mm x 219mm x 60mm
* poids : 262g
* materials: 
  * casing and internal structure are made of birch plywood
  * the light diffuser (between leds and letters) is made of recycled paper