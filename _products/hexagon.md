---
layout: product
title: "Hexagon"
category: horloge wifi
lang: fr
ref: hexagon
---

*Hexagon* est une horloge qui donne l'heure via des hexagones lumineux.


Configurable, elle se met à l’heure et suit les changements d’horaires automatiquement via une connexion wifi à internet.

Elle utilise 88 leds pour afficher l'heure (heures, minutes et secondes). La couleur des chiffres changent lentement 

D'autres fonctionnalités s’ajouteront peut-être au fil du temps, mais rien ne le garantie.

## open source

L’ensemble du projet (code source, plan du boitier et schéma électronique) est sous licence open source et disponible sur [gitlab](https://gitlab.com/avernois/clocks).

La mise à jour automatique de l’heure via le protocol NTP en fonction du fuseau horaire configuré s'appuie sur le projet [ZoneTransition](https://gitlab.com/avernois/zone-transition).
C'est lui qui gère le décalage par rapport à l’heure UTC et les dates des changements d’heure été/hiver.

Les contributions sont les bienvenues et jouer avec la plateforme est fortement encouragé. Pour cela, les pins permettant le reprogrammation du micro controleur sont accessible (nécessite un convertisseur usb<->serial non inclu).

## fabrication et délais

*Heagon* est fabriquée par mes soins dans mon atelier à la demande. Selon si j'ai les composants en stock et les autres projets en cours, les délais de fabrication peuvent varier de quelques jours à 2 mois.

## caractéristiques techniques

* alimentation : 5v 500mA (ou supérieur) avec un connecteur micro USB. L'alimentation n'est pas fournie.
* wifi : 802.11 b/g/n (2.4gHz uniquement)
* dimension : 277mm x 127mm x 60mm
* poids : 275g
* matériaux :
  * le boitier et la structure interne sont en contreplaqué de bouleau
  * le diffuseur (entre les leds et les lettres) est en papier recyclé