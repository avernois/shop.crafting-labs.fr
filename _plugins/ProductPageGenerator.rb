module Reader
    class Generator < Jekyll::Generator
        safe true
  
        def generate(site)
            site.collections['products'].docs.each do |product|
                page = ProductPage.new(site, product)
                site.pages << page
            end
        end
    end
  
    class ProductPage < Jekyll::Page
        def initialize(site, product)
            @site = site             # the current site instance.
            @base = site.source      # path to the source directory.
            @ref = product.data['ref']
            @lang = product.data['lang']
            
            @basename = 'index'      # filename without the extension.
            @ext      = '.md'      # the extension.
            @name     = 'index.html' # basically @basename + @ext.
            
            @data = {
                "permalink" => "/:lang/products/:name/",
                "layout"    => "product",
                "lang"      => product.data["lang"],
                "ref"       => product.data["ref"],
                "title"     => product.data["title"]
            }

            @content = product.content
        end
    
        # Placeholders that are used in constructing page URL.
        def url_placeholders
            {
                :name => @ref,
                :category   => @dir,
                :basename   => basename,
                :output_ext => output_ext,
                :lang => @lang
            }
        end
    end
end
