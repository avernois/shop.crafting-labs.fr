module OrderPage
    class OrderPageGenerator < Jekyll::Generator
        safe true
  
        def generate(site)
            site.collections['products'].docs.each do |product|
                page = OrderPage.new(site, product)
                site.pages << page
            end
      end
    end
  
    class OrderPage < Jekyll::Page
      def initialize(site, product)
        @site = site             # the current site instance.
        @base = site.source      # path to the source directory.
        @ref = product.data['ref']
        @lang = product.data['lang']
        
        @basename = 'index'      # filename without the extension.
        @ext      = '.html'      # the extension.
        @name     = 'index.html' # basically @basename + @ext.
  
        @data = {
          'title' => product.data['title'],
          'layout' => "buy",
          'permalink' => ":lang/products/:name/buy/",
          'category' => product.data['category'],
          'ref' => product.data['ref'],
          'lang' => product.data['lang'],
          'scripts' => 'https://js.stripe.com/v3/'
        }
      end
  
      # Placeholders that are used in constructing page URL.
      def url_placeholders
        {
          :name => @ref,
          :category   => @dir,
          :basename   => basename,
          :output_ext => output_ext,
          :lang => @lang
        }
      end
    end
  end