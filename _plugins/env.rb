module Jekyll

    class EnvironmentVariablesGenerator < Generator
  
      def generate(site)
        site.config['stripe_public_key'] = ENV['STRIPE_PUBLIC_KEY'] || 'development'
        site.config['pay_server'] = ENV['PAY_SERVER'] || 'http://localhost:4567'
      end
  
    end
    
  end